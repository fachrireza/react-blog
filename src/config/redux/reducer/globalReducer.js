const initialState = {
    btn_name: 'create new post'
}

const globalReducer = (state = initialState, action) => {

    if (action.type === 'UPDATE_OTHER') {
        return{
            ...state,
            btn_name: 'create new blog'
        }
    }

     return state;
}

export default globalReducer