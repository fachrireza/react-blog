import  Axios  from "axios";

export const setDataBlog = (page) => (dispatch) => {
    // return (dispatch) => {
        Axios.get(`http://localhost:4000/api/v1/blog/posts?page=${page}&perPage=4`)
        .then(result => {
            const responseData = result.data;
            dispatch({ 
                    type: 'UPDATE_DATA', 
                    payload: responseData.data 
                })

            dispatch({ 
                type: 'UPDATE_PAGE', 
                payload: { 
                    currentPage: responseData.current_page, 
                    totalPage:  Math.ceil(responseData.total_items / responseData.per_page)
                }
            })
        })
        .catch(err => {
            console.log('error :', err);
        })
    // }

    // return {
    //     type: 'UPDATE_DATA', payload
    // }
}

// export default setDataBlog