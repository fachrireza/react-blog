import React from 'react';
import { Routes, Store } from '../config';
import './App.css';
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={Store}>
        <Routes/>
    </Provider>

  );
}

export default App;
