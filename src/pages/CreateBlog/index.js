import React, { useEffect, useState } from 'react'
import { Button, Gap, Input, Link, TextArea, Upload } from '../../components'
import { Editor } from '@tinymce/tinymce-react';
import {useHistory, withRouter} from 'react-router-dom'
// import Axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { postToApi, updateToApi, setForm, setImgPreview } from '../../config/redux/action';
import axios from 'axios';

function CreateBlog(props) {
    const {form, imgPreview} = useSelector(state => state.createBlogReducer)
    const {title, body} = form;
    const [isUpdate, setIsUpdate] = useState(false)
    const dispatch = useDispatch()
    const history = useHistory();
    

    // const [title, setTitle] = useState('');
    // const [image, setImage] = useState('');
    // const [body, setBody] = useState('');
    // const [previewImage, setPreviewImage] = useState(null)
    useEffect(() => {
        console.log('params :' , props);
        const id = props.match.params.id
        if (id) {
            setIsUpdate(true);
            axios.get(`http://localhost:4000/api/v1/blog/post/${id}`)
            .then(result => {
                const data = result.data.data;
                console.log('result :' , result);
                dispatch(setForm('title', data.title))
                dispatch(setForm('body', data.body))
                dispatch(setImgPreview(`http://localhost:4000/${data.image}`))
            })
            .catch(error => {
                console.log('error :' , error);
            })
        }
    }, []);


    const submit = () => {
        if (isUpdate) {
            const id = props.match.params.id
            console.log('update data');
            updateToApi(form, id)
        }else{
            console.log('post data');
            postToApi(form);
        }
        // console.log(title);
        // console.log(body);
        // console.log(image);
        // const data = new FormData();
        // data.append('title', title);
        // data.append('body', body);
        // data.append('image', image);

        // Axios.post('http://localhost:4000/api/v1/blog/post', data, {
        //     headers: {
        //         'content-type':'multipart/form-data'
        //     }
        // }).
        // then((result) => {
        //     // console.log(result);
        //     alert('Posting artikel berhasil')
        //     window.location.replace('/')
        // }).catch((err) => {
        //     console.log(err);
        //     console.log(data);
        // });
    }
    const imageFileUploaded = (e) => {
        const file = e.target.files[0]
        dispatch(setForm('image' , file));
        dispatch(setImgPreview(URL.createObjectURL(file)))
        // setImage(file);
        // setPreviewImage(URL.createObjectURL(file))
    }
    return (
        <div className="blog-post">
            <Link title="<- Kembali" onClick={() => history.push('/')}/>
            <p>{isUpdate ? 'Update' : 'Create New'} Blog Post</p>
            <Input 
                label="Post Title :" 
                value={title}
                onChange={(e) => dispatch(setForm('title', e.target.value))}
                // onChange={(e) => setTitle(e.target.value)}
            />
            <Upload 
                label="Upload Image :" 
                onChange={(e) => imageFileUploaded(e)}
                img={imgPreview}
                // img={previewImage}

            />
            <p>Write your post :</p>
            <Editor
                apiKey="n05ozx9drnwrklghf3soyr3qodz9lsuhyfdy79ib22dt5sqx"
                initialValue={body}
                onChange={(e) => dispatch(setForm('body', e.target.getContent({ format: "text" })))}
                // onChange={(e) => setBody(e.target.getContent({ format: "text" }))}
                init={{
                    height: 500,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount'
                    ],
                    toolbar:
                    'undo redo | formatselect | bold italic backcolor | \
                    alignleft aligncenter alignright alignjustify | \
                    bullist numlist outdent indent | removeformat | help'
                }}
            />
            <Gap height={20}/>
            <Button title={isUpdate ? 'Update' : 'Submit'} onClick={submit}/>
            <Gap height={20}/>
            {/* <TextArea  
                value={body}
                onChange={(e) => setBody(e.target.value)}
            /> */}
        </div>
    )
}

export default withRouter(CreateBlog)
