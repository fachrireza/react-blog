import React, { useEffect, useState } from 'react'
import { ImageContent3 } from '../../assets'
import { Gap, Link } from '../../components'
import './detailblog.scss'
import { useHistory, withRouter } from "react-router-dom";
import axios from 'axios';

function DetailBlog(props) {
    const [data, setData] = useState({})
    useEffect(() => {
        const id = props.match.params.id
        axios.get(`http://localhost:4000/api/v1/blog/post/${id}`)
        .then(result => {
            setData(result.data.data)
        })
        .catch(error => {
            console.log('error', error);
        })
    }, [props])
    const history = useHistory()
    if (data.author) {
        return (
            <div className="detail-blog-wrapper">
                <Link title="<- Kembali" onClick={() => history.push('/blogitem')} />
                <Gap height={20}/>
                <img className="img-cover" src={`http://localhost:4000/${data.image}`} alt="thumb"/>
                <p className="blog-title">{data.title}</p>
                <p className="blog-author">{data.author.name} - {data.createdAt}</p>
                <p className="blog-body">{data.body}</p>
            </div>
        )
    }
    return <p>Loading content ...</p>
        
}

export default withRouter(DetailBlog)
