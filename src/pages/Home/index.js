import React, { useEffect, useState } from 'react' //useState for local state
import  Axios  from "axios";
import { Button, Gap } from '../../components'
import BlogItem from '../../components/molecules/BlogItem'
import {useHistory} from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";
import './home.scss'
import { setDataBlog } from '../../config/redux/action';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function Home() {
    const [counter, setCounter] = useState(1)
    const {dataBlog, page} = useSelector(state => state.homeReducer) // global state
    
    // console.log(page);
    // const [dataBlog, setDataBlog] = useState([]) // local state
    // console.log('globalState = ', dataBlogs);
    // const {dataBlogs, btn_name} =  useSelector(state => state)
    // console.log('dataBlogGlobalState = ', dataBlog);

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setDataBlog(counter))
        // console.log('default:' , counter);
        // Axios.get('http://localhost:4000/api/v1/blog/posts')
        // .then(result => {
        //     const responseData = result.data;
        //     dispatch(setDataBlog(responseData.data))
        //     // console.log('data :' , result.data);
        //     // dispatch({type: 'UPDATE_DATA', payload: responseData.data}) // call from global state
        //     // setDataBlog(responseData.data) // call from local state
        // })
        // .catch(err => {
        //     console.log('error :', err);
        // })
    }, [counter, dispatch])
    
    const history = useHistory();

    const previous = () => {
        setCounter(counter <= 1 ? 1 : counter - 1)
        // console.log('prev : ', counter);
    }

    const next = () => {
        setCounter(counter === page.totalPage ? page.totalPage : counter + 1)
        // console.log('next : ', counter);
    }

    const confirmDelete = (id) => {
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Apakah Anda yakin menghapus posting blog?',
            buttons: [{
                label: 'Ya',
                onClick: () => {
                    Axios.delete(`http://localhost:4000/api/v1/blog/post/${id}`)
                    .then(result => {
                        // console.log('success delete :', result.data);
                        dispatch(setDataBlog(counter))
                    })
                    .catch(error => {
                        console.log(error);
                    })
                }
            },{
                label: 'Tidak',
                onClick: () => console.log('user tidak setuju')
            }
            ]
        })
    }

    return (
        <div className="home-page-wrapper">
            <div className="create-wrapper">
                <Button title="create new blog" onClick={() => history.push('/create-blog')}/>
            </div>
            <Gap height={20} />
            <div className="content-wrapper">
                {dataBlog.map(blog => {
                    return <BlogItem 
                                key={blog._id} 
                                image={`http://localhost:4000/${blog.image}`}
                                title={blog.title}
                                body={blog.body}
                                name={blog.author.name}
                                date={blog.createdAt}
                                _id={blog._id}
                                onDelete={confirmDelete}
                            />
                })}
            </div>
            <div className="pagination">
                <Button title="Previous" onClick={previous} />
                <Gap width={20}/>
                <p className='text-pageOf'>{page.currentPage} / {page.totalPage}</p>
                <Gap width={20}/>
                <Button title="Next" onClick={next} />
            </div>
            <Gap height={20}/>
        </div>
    )
}

export default Home
