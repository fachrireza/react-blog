import React from 'react'
import { LoginBg } from '../../assets';
import { Button, Gap, Input, Link } from '../../components';
import { useHistory } from "react-router-dom";

function Login() {
    const history = useHistory();
    return (
        <div className="main-page">
            <div className="left">
                <img src={LoginBg} className="bg-image" alt="img-bg" />
            </div>
            <div className="right">
                <p className="title">Login</p>
                <Input label="Email :" placeholder="Email"/>
                <Gap height={20}/>
                <Input label="Password :" placeholder="Password"/>
                <Gap height={50}/>
                <Button title="login"/>
                <Link textTitle="Belum punya akun? Silahkan " title="DAFTAR." url="register" 
                onClick={() => history.push('/register')}/>
            </div>
        </div>
    )
}

export default Login
