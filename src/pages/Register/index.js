import React from 'react'
import { RegisterBg } from '../../assets';
import { Button, Gap, Input, Link } from '../../components';
import './register.scss';
import { useHistory } from "react-router-dom";

function Register() {
    const history = useHistory();
    return (
        <div className="main-page">
            <div className="left">
                <img src={RegisterBg} className="bg-image" alt="img-bg" />
            </div>
            <div className="right">
                <p className="title">Registrasi</p>
                <Input label="Full Name :" placeholder="Full Name"/>
                <Gap height={20}/>
                <Input label="Email :" placeholder="Email"/>
                <Gap height={20}/>
                <Input label="Password :" placeholder="Password"/>
                <Gap height={50}/>
                <Button title="submit"/>
                <Link textTitle="Sudah punya akun? Silahkan " 
                title="LOGIN." 
                onClick={() => history.push('/login')}/>
            </div>
        </div>
    )
}

export default Register
