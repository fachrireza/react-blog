import RegisterBg from './image/register-bg.jpg';
import LoginBg from './image/login-bg.jpg';
import LogoBlog from './logo/logo_client_white.png';
import ImageContent1 from './image/img-content-1.jpg'
import ImageContent2 from './image/img-content-2.jpg'
import ImageContent3 from './image/img-content-3.jpg'

export {RegisterBg, LoginBg, LogoBlog, ImageContent1, ImageContent2, ImageContent3};