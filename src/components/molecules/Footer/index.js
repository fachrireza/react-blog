import React from 'react'
import './footer.scss'
import { LogoBlog } from '../../../assets'

function Footer() {
    return (
        <div>
            <div className="footer">
                <div>
                    <img className="logo" src={LogoBlog}/>
                </div>
            </div>
            <div className="copyright">
                <p>Copyright &#169;2021</p>
            </div>
        </div>
    )
}

export default Footer