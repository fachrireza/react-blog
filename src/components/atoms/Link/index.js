import React from 'react'
import './link.scss'

function Link({textTitle, title, onClick, url}) {
    return (
        <div>
            <p className="textLink">{textTitle}<a className="link" href={url} onClick={onClick}>{title}</a></p>
        </div>
    )
}

export default Link
