import React from 'react'
import { ImageContent3 } from '../../../assets'
import './upload.scss'

function Upload({img, label, ...rest}) {
    return (
        <div className="upload">
            <p className="label">{label}</p>
            {img && <img className="preview" src={img} alt="preview"/>}
            <input type="file" {...rest}/>
        </div>
    )
}

export default Upload
